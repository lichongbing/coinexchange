package com.xxll.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xxll.service.WorkIssueService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxll.domain.WorkIssue;
import com.xxll.mapper.WorkIssueMapper;
import org.springframework.util.StringUtils;

@Service
public class WorkIssueServiceImpl extends ServiceImpl<WorkIssueMapper, WorkIssue> implements WorkIssueService {

    /**
     * <h2>分页条件查询工单</h2>
     * @param page       分页参数
     * @param status     工单当前的处理状态
     * @param startTime  工单创建的起始时间
     * @param endTime    工单创建的截至时间
     **/
    @Override
    public Page<WorkIssue> findByPage(Page<WorkIssue> page, Integer status, String startTime, String endTime) {
        return page(page, new LambdaQueryWrapper<WorkIssue>()
                .eq(status != null, WorkIssue::getStatus, status)
                .between(
                        !StringUtils.isEmpty(startTime) && !StringUtils.isEmpty(endTime),
                        WorkIssue::getCreated,
                        startTime,
                        endTime + " 23:59:59"
                )
        );
    }
}
