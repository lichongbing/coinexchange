package com.xxll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxll.domain.AdminBank;

public interface AdminBankMapper extends BaseMapper<AdminBank> {
}